<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
	/**
	 * @var string
	 */
	private $defaultLocale;

	public function __construct(string $defaultLocale = 'en')
	{
		$this->defaultLocale = $defaultLocale;
	}

	public static function getSubscribedEvents(): array
	{
		return [
			KernelEvents::REQUEST => [
				[
					'onKernelRequest',
					20,
				],
			],
		];
	}

	public function onKernelRequest(GetResponseEvent $event): void
	{
		$request = $event->getRequest();

		if (!$request->hasPreviousSession()) {
			return;
		}

		$locale = $request->attributes->get('_locale');
		if (!empty($locale)) {
			$request->getSession()->set('_locale', $locale);
		} else {
			$locale = $request->getSession()->get('_locale', $this->defaultLocale);
			$request->setLocale($locale);
		}
	}
}
