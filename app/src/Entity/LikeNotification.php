<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeNotificationRepository")
 */
class LikeNotification extends Notification
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MicroPost", inversedBy="likeNotifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $microPost;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="likeNotifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $likedBy;

    public function getMicroPost(): ?MicroPost
    {
        return $this->microPost;
    }

    public function setMicroPost(?MicroPost $microPost): self
    {
        $this->microPost = $microPost;
        return $this;
    }

    public function getLikedBy(): ?User
    {
        return $this->likedBy;
    }

    public function setLikedBy(?User $likedBy): self
    {
        $this->likedBy = $likedBy;
        return $this;
    }
}
