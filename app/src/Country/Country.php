<?php

namespace App\Country;

interface Country {

	public function getCurrency(): string;

	public function getCurrencyThounsandsSeparator(): string;

	public function getCurrencyDecimalPoint(): string;
}
