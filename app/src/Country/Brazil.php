<?php

namespace App\Country;

class Brazil implements Country
{
	/** @var string */
	private $currency;
	/** @var string */
	private $currencyThounsandsSeparator;
	/** @var string */
	private $currencyDecimalPoint;

	public function __construct()
	{
		$this->currency = 'R$';
		$this->currencyThounsandsSeparator = '.';
		$this->currencyDecimalPoint = ',';
	}

	public function getCurrency(): string
	{
		return $this->currency;
	}

	public function getCurrencyThounsandsSeparator(): string
	{
		return $this->currencyThounsandsSeparator;
	}

	public function getCurrencyDecimalPoint(): string
	{
		return $this->currencyDecimalPoint;
	}
}
