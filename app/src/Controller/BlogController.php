<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class BlogController
{
	/** @var Twig_Environment */
	private $twig;

	public function __construct(\Twig_Environment $twig)
	{
		$this->twig = $twig;
	}

	/**
	 * @Route("/", name="blog_index")
	 */
	public function index(): Response
	{
		$html = $this->twig->render('base.html.twig');
		return new Response($html);
	}

	/**
	 * @Route("/show", name="blog_show")
	 */
	public function show()
	{
		throw new NotFoundHttpException("Could not found the requested post");
	}
}
