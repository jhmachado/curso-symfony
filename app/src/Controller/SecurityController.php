<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController
{
	/** @var \Twig_Environment */
	private $twig;

	public function __construct(\Twig_Environment $twig)
	{
		$this->twig = $twig;
	}

	/**
	 * @Route("/login", name="security_login")
	 */
	public function login(AuthenticationUtils $authenticationUtils): Response
	{
		$responseBody = $this->twig->render('security/login.html.twig', [
			'last_username' => $authenticationUtils->getLastUserName(),
			'error' => $authenticationUtils->getLastAuthenticationError(),
		]);
		
		return new Response($responseBody);
	}

	/**
	 * @Route("/logout", name="security_logout")
	 */
	public function logout(): Response
	{}

	/**
	 * @Route("/confirm/{token}", name="security_confirm")
	 */
	public function confirm(
		UserRepository $userRepository,
		EntityManagerInterface $entityManager,
		string $token
	): Response
	{
		$user = $userRepository->findOneBy([
			'confirmationToken' => $token,
		]);

		if (!is_null($user)) {
			$user->setEnabled(true);
			$user->setConfirmationToken('');
			$entityManager->flush();
		}

		$responseBody = $this->twig->render('security/confirmation.html.twig', [
			'user' => $user,
		]);

		return new Response($responseBody);
	}
}