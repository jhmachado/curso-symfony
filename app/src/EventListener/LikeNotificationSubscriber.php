<?php

namespace App\EventListener;

use App\Entity\LikeNotification;
use App\Entity\MicroPost;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;

class LikeNotificationSubscriber implements EventSubscriber
{
	public function getSubscribedEvents(): array
	{
		return [
			Events::onFlush,
		];
	}

	public function onFlush(OnFlushEventArgs $args)
	{
		$entityManager = $args->getEntityManager();
		$unityOfWork = $entityManager->getUnitOfWork();

		$updates =  $unityOfWork->getScheduledCollectionUpdates();
		foreach ($updates as $update) {
			if (!$update->getOwner() instanceof MicroPost) {
				continue;
			}

			$mapping = $update->getMapping();
			if ($mapping['fieldName'] !== 'likedBy') {
				continue;
			}

			$insertDiff = $update->getInsertDiff();
			if (!count($insertDiff)) {
				return;
			}

			$microPost = $update->getOwner();
			$notification = new LikeNotification();
			$notification->setUser($microPost->getUser());
			$notification->setMicroPost($microPost);
			$notification->setLikedBy(reset($insertDiff));

			$entityManager->persist($notification);
			$unityOfWork->computeChangeSet(
				$entityManager->getClassMetaData(LikeNotification::class),
				$notification
			);
		}
	}
}
