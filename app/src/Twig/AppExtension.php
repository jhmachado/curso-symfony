<?php

namespace App\Twig;

use App\Country\Country;
use App\Country\Brazil;
use App\Country\UnitedStates;
use App\Entity\LikeNotification;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension implements GlobalsInterface
{
	/** @var string */
	private $locale;

	public function __construct(string $locale)
	{
		$this->locale = $locale;
	}

	public function getFilters(): array
	{
		return [
			new TwigFilter('price', [$this, 'priceFilter']),
		];
	}

	public function getGlobals(): array
	{
		return [
			'locale' => $this->locale,
		];
	}

	public function priceFilter(string $number): string
	{
		$country = $this->resolveCountryByLocale();
		$formated_value = number_format($number, 2, $country->getCurrencyDecimalPoint(), $country->getCurrencyThounsandsSeparator());
		return "{$country->getCurrency()} {$formated_value}";
	}

	private function resolveCountryByLocale(): Country
	{
		$countries = [
			'en' => new UnitedStates(),
			'pt_br' => new Brazil(),
		];

		if (!isset($countries[$this->locale])) {
			throw new \LogicException('Could not find a country mapped for this locale');
		}

		return $countries[$this->locale];
	}

	public function getTests(): array
	{
		return [
			new \Twig_SimpleTest('like', function ($object) {
				return $object instanceof LikeNotification;
			})
		];
	}
}
